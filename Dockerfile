FROM java:8

WORKDIR /home/root/javahelloworld
#COPY HelloWorld.java /
COPY src /home/root/javahelloworld/src

#RUN javac HelloWorld.java
RUN mkdir bin && javac -d bin src/HelloWorld.java

#ENTRYPOINT ["java", "HelloWorld"]
ENTRYPOINT ["java", "-cp", "bin", "HelloWorld"]
